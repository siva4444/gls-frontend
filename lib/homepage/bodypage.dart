import 'package:flutter/material.dart';

class GLSbodyPage extends StatelessWidget {
  const GLSbodyPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: Container(
                padding: EdgeInsets.all(15),
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.white10,
                child: ListView(children: [
                  // About us
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.red,
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "About Us",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset("images/aboutus-img.png",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),
                  SizedBox(height: 15.0,),
                  // News Letter
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.orange[400],
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Newsletter",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset("images/aboutus-img.png",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),
                  SizedBox(height: 15.0,),
                  // Webinor
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.purple,
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Webinor",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset(
                                    "images/audit-assurance-services-inside.jpg",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),
                  SizedBox(height: 15.0,),
                  // corporatelaw
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.green[300],
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Corporate Law \nSecretarial \nSupport",
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white, fontWeight: FontWeight.w800
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset(
                                    "images/corporate-law-secretarial-support-inside.jpg",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),
                  SizedBox(height: 15.0,),
                  // goods and service tax
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.pink,
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Goods And\nService tax",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset("images/aboutus-img.png",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),SizedBox(height: 15.0,),
                  // Litigation support
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.grey,
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Litigation Support\nServices",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset("images/litigation-support-services-inside.jpg",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),
                  SizedBox(height: 15.0,),
                  // Direct Taxes
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.indigo[200],
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Direct Taxes",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset("images/income-tax-inside.jpg",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),
                  SizedBox(height: 15.0,),
                  // cost audit
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.lime[700],
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Cost Audit And\nComplaince\nReport",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset("images/import-export-compliances-inside.jpg",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),
                  SizedBox(height: 15.0,),
                  // Foreign Trade
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.orange,
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Foreign Trade\nPolicy Dgft\nServices",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset("images/dgft-inside.jpg",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),
                  SizedBox(height: 15.0,),
                  // Exim compliances
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.purple[100],
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Exim\ncompliances",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset("images/exim-compliances.png",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),
                  SizedBox(height: 15.0,),
                  // Statutory Audit, Internal Audit And Other Assurance Services
                  ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Card(
                        color: Colors.orange[200],
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 25.0),
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Statutory Audit\nInternal Audit And\nOther Assurance\nServices",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 75,
                              top: 10,
                              child: Container(
                                height: 180,
                                width: MediaQuery.of(context).size.width,
                                child: Image.asset("images/audit-assurance-services-inside.jpg",
                                    fit: BoxFit.contain),
                              ),
                            ),
                            Positioned(
                                top: 120,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(15, 20, 10, 10),
                                  child: RaisedButton(
                                      child: Text(
                                        "Know More",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      ),
                                      color: Colors.grey[600],
                                      onPressed: () {}),
                                ))
                          ],
                        )),
                  ),
                ]))),
      ],
    );
  }
}
