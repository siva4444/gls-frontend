import 'package:GLS_UI/homepage/bodypage.dart';
import 'package:flutter/material.dart';

class GLSHomePage extends StatefulWidget {
  GLSHomePage({Key key}) : super(key: key);

  @override
  GLSHomePageState createState() => GLSHomePageState();
}

class GLSHomePageState extends State<GLSHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.pink[50],
            title: Center(
                child: Text(
              "GLS\nGST LEGAL SERVICES",
              textAlign: TextAlign.center,
            style: TextStyle(color: Colors.blue),)),
            leading: GestureDetector(onTap: () {}, child: Icon(Icons.menu, color: Colors.blueAccent,),),
            actions: <Widget>[
          Padding(
              padding: const EdgeInsets.fromLTRB(0, 15, 25, 0),
              child: GestureDetector(onTap: () {}, child: Text("Login", style: TextStyle(
                color: Colors.blueAccent, fontSize: 20
              )),)),
        ]),
        body: GLSbodyPage(),
        );
  }
}
